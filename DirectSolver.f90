module DirectSolve
implicit none

contains

subroutine CollocationRHSComputation(rhs_NK,DNK,rhs_L)
    implicit none
    real*8, intent(in) :: rhs_NK(:,:),DNK(:,:)
    real*8, intent(inout) :: rhs_L(:,:)

    integer :: N,K,L, ii, jj, nn
    N=ubound(rhs_NK,1)
    K=ubound(rhs_NK,2)
    L = N * K !(N-1 * K-1)?

    do jj=1,K
        do ii=1,N
            nn=ii+(jj-1)*(N-1)
            !rhs_L(nn)=

        end do
    end do

end subroutine CollocationRHSComputation

subroutine LaplaceCollocationMatrix
    implicit none

end subroutine LaplaceCollocationMatrix

end module DirectSolve
