M�thode �l�ments spectraux: 1D
Diffusion 1D�:

Bas� sur Implementing Spectral Methods for Partial Differential Equations�: Kopriva 2009

Le programme reprend les routines du chapitre 8-8.1.1-8.1.2 afin de r�soudre une �quation de diffusion.

Un intervalle fini [x0, xK] est d�coup� en K sous-intervalle {I1, �, Ik} qui sont les �l�ments spectraux, ek.
La d�composition en sous-intervalle permet de d�couper les int�grales de la formulation faible du probl�me � r�soudre. Et ensuite on applique une m�thode spectrale sur chaque segment du domaine (avec une transformation de l�intervalle [xi-1,xi] en [-1,1]) pour une r�solution � partir de la base polynomiale choisie ( Legendre Gauss-Lobatto, LGL, par exemple).

Le code�:

Dans le main�:  
-Construction du domaine et des sous domaines, variable NbDom & xFrontDom.  
-s�lection de la base polynomiale. Pour l�instant, juste LGL.  
-cr�ation de la base polynomiale et de la matrice de d�rivation  
-cr�ation des �l�ments pour le solver.  
-boucle en temps�:  
	- avance en temps (avec matriceAction +1.)  
	- calcul des valeurs aux fronti�res  
	- r�solution avec gradient conjugu�  