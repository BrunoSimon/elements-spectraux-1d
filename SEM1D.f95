!Spectral element method 1D
module SEM1D
implicit none

private !all privates except next
public :: constructSEM1D, printInfo
public :: sol_Init, xReal
public :: MatrixAction, SetBoundaryConditions, UnMask, Residual
public :: sol_NK, rhs_NK

type sharedNodePtrs
    integer :: eLeft
    integer :: eRight
    integer :: nodeLeft
    integer :: nodeRight
end type sharedNodePtrs

!attribut
integer :: NbSubDom    !Number of element or subdomain K

real*8, allocatable :: xel_K(:)    !0-K
real*8, allocatable :: deltax_K(:) ! 1-K (xel_k(1)-xel_k(k-1)
real*8, allocatable :: sol_NK(:,:)  !0-N,1-K
real*8, allocatable :: rhs_NK(:,:)  !0-N,1-K
type(sharedNodePtrs), allocatable :: p_K(:) !1-k, shared node pointer

contains
!procedure -------------------------------

!-------------------
subroutine constructSEM1D(N,K,xfront,quadMet)
    !Create variable for SEM
    use quadrature, only: NodesAndWeight
    use lagrangePoly, only : CGDerivativeMatrix
    implicit none

    integer, intent(in) :: N, K
    real*8 , intent(in) :: xfront(0:)
    character(len=10),intent(in) :: quadMet
    !Initialize the mesh and derivative

    integer :: kk

    NbSubDom=K

    !create nodes and weight of quadrature
    call NodesAndWeight(N,quadMet)
    !create GNN matrix
    call CGDerivativeMatrix(N)

    allocate(xel_k(0:NbSubDom))
    xel_k=xfront

    allocate(deltax_k(NbSubDom))
    do kk=1,NbSubDom
        deltax_k(kk)=xel_k(kk)-xel_k(kk-1)
    end do

    allocate(p_k(NbSubDom-1))
    do kk=1,NbSubDom-1
        p_k(kk)%eLeft=kk
        p_k(kk)%eRight=kk+1
        p_k(kk)%nodeLeft=N
        p_k(kk)%nodeRight=0
    end do
    allocate(sol_NK(0:N,NbSubDom))
    allocate(rhs_NK(0:N,NbSubDom))

end subroutine constructSEM1D
!------------------------


!------------------------
subroutine printInfo()
    !Print recap
    use quadrature, only: printNodesAndWeights

    integer ::kk

    print *, "Number of element:", NbSubDom
    do kk=1,NbSubDom
        print *, "Element:",kk
        print *, "Intervalle:",xel_k(kk-1),xel_k(kk)
        print *, 'Length:',deltax_k(kk)
        if (kk<NbSubDom) then
            print *, "Voisin gauche", p_k(kk)%eLeft
            print *, "Voisin droit", p_k(kk)%eRight
            print *, "Noeud gauche", p_k(kk)%nodeLeft
            print *, "Noeud droit", p_k(kk)%nodeRight
        end if
    end do

    call printNodesAndWeights()

end subroutine printInfo
!-------------------------------------


!------------------------
subroutine Mask(aNK)
    !Mask that make the shared node on the right at 0
    implicit none

    real*8,intent(inout) :: aNK(0:,1:)

    integer :: kk, jR, eR

    do kk=1,NbSubDom-1
        jR=p_k(kk)%nodeRight
        eR=p_k(kk)%eRight
        aNK(jR,eR)=0.d0
    end do

end subroutine Mask
!------------------------

!------------------------
subroutine UnMask(aNK)
    implicit none
    real*8,intent(inout) :: aNK(0:,1:)

    integer :: kk, jR, jL, eR, eL

    do kk=1,NbSubDom-1
        jR=p_k(kk)%nodeRight
        jL=p_k(kk)%nodeLeft
        eR=p_k(kk)%eRight
        eL=p_k(kk)%eLeft
        aNK(jR,eR)=aNK(jL,eL)
    end do
end subroutine UnMask
!------------------------

!------------------------
subroutine GlobalSum(aNK)
    implicit none
    real*8,intent(inout) :: aNK(0:,1:)

    integer :: kk, jR, jL, eR, eL
    real*8 :: tmp

    do kk=1,NbSubDom-1
        jR=p_k(kk)%nodeRight
        jL=p_k(kk)%nodeLeft
        eR=p_k(kk)%eRight
        eL=p_k(kk)%eLeft
        tmp=aNK(jR,eR)+aNK(jL,eL)
        aNK(jR,eR)=tmp
        aNK(jL,eL)=tmp
    end do

end subroutine GlobalSum
!------------------------

!------------------------
function MxVDerivative(D,F)
    !Multiply a matrice with a vector, used for the derivative matrice
    !simple product matrix vector
    use quadrature, only : NbNode
    implicit none
    real*8, intent(in) :: D(:,:), f(:)

    real*8 :: MxVDerivative(0:NbNode)
    MxVDerivative=matmul(D,F)
end function MxVDerivative
!------------------------

!------------------------
subroutine LaplaceOperator(UNK,DNK)
    !Spacial approximation for the one dimension
    use quadrature, only : getNbNode
    use lagrangePoly, only : GNN
    implicit none
    real*8, intent (in) :: UNK(0:,1:)
    real*8, intent (out):: DNK(0:,1:)

    integer :: kk,N,K

    N=getNbNode()
    K=NbSubDom

    do kk=1, K
        DNK(:,kk)=-2.d0/deltax_k(kk)*MxVDerivative(GNN,UNK(:,kk))
    end do
end subroutine LaplaceOperator
!------------------------

!------------------------
subroutine MatrixAction(s,deltat,UNK,AUNK)
    use quadrature, only : getNbNode, weight_n
    implicit none
    real*8, intent(in) :: s
    real*8, intent(in) :: deltat
    real*8, intent(inout) :: UNK(0:,1:)
    real*8, intent(out) :: AUNK(0:,1:)

    integer :: N,K,jj,kk
    N=getNbNode()
    K=NbSubDom

    call UnMask(UNK)

    call LaplaceOperator(UNK,AUNK)

    do kk=1,K
        do jj=0,N
            AUNK(jj,kk)=0.5d0*weight_n(jj)*deltax_k(kk)*UNK(jj,kk) + s * 0.5d0*deltat*AUNK(jj,kk)
        end do
    end do

    call GlobalSum(AUNK)

    call Mask(UNK)

    if  (s<0) then
        call Mask(AUNK)
        AUNK(0,1)=0.d0
        AUNK(N,K)=0.d0
    end if
end subroutine MatrixAction
!------------------------

!------------------------
subroutine Residual(UNK,res_NK, deltat)
    use quadrature, only : getNbNode
    implicit none
    real*8, intent(inout) :: UNK(0:,1:)
    real*8, intent(in) :: deltat
    real*8, intent(out) :: res_NK(0:,1:)

    integer :: N,K
    N=getNbNode()
    K=NbSubDom

    call MatrixAction(-1.d0,deltat,UNK,res_NK)

    res_NK= rhs_NK - res_NK

    call Mask(res_NK)

    res_NK(0,1)=0.d0
    res_NK(N,K)=0.d0

end subroutine
!------------------------

!------------------------
subroutine SetBoundaryConditions (UNK,t)
    use quadrature, only : getNbNode
    implicit none
    real*8, intent(inout) :: UNK(0:,1:)
    real*8, intent(in) :: t

    integer :: N,K

    N=getNbNode()
    K=NbSubDom
    UNK(0,1)=exp(-xel_K(0)**2/(4.d0*t+1.d0))/ sqrt(4.d0*t+1.d0)
    UNK(N,K)=exp(-xel_K(K)**2/(4.d0*t+1.d0))/ sqrt(4.d0*t+1.d0)

end subroutine SetBoundaryConditions
!------------------------

!------------------------
subroutine sol_Init(UNK)
    !compute the initial solution
    use quadrature, only : getNbNode
    implicit none
    real*8, intent (out) :: UNK(0:,:)

    integer :: N, kk,jj
    N=getNbNode()

    do kk=1,NbSubDom
        do jj=0, N
            UNK(jj,kk)=exp(-(xreal(jj,kk))**2)
        end do
    end do

end subroutine sol_Init
!------------------------

!------------------------
real*8 function xReal(num_node,num_el)
    use quadrature, only: getXNoden
    implicit none

    integer, intent(in) :: num_node,num_el
    xreal=xel_K(num_el-1) + (getXNoden(num_node)-getXNoden(0))*0.5d0*deltax_K(num_el)
end function
!------------------------
end module SEM1D
!------------------------
