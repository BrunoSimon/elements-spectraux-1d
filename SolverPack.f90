module SolverPack
    !only ConjugateGradientSolve works and is used

    implicit none
    private
    public :: ConjugateGradientSolve, constructFD

    integer :: Nbx
    integer :: Nby
    real*8, allocatable :: aij(:,:)
    real*8, allocatable :: dxi(:)
    real*8, allocatable :: dyj(:)

contains

subroutine constructFD(N,M,xi,yj)
    !not working
    implicit none
    integer, intent(in) :: N,M
    real*8, intent(in) :: xi(0:N), yj(0:M)

    integer :: ii,jj

    Nbx=N
    Nby=M
    allocate(dxi(Nbx))
    allocate(dyj(Nby))

    dxi=xi(1:Nbx)-xi(0:Nbx-1)
    dyj=yj(1:Nby)-yj(0:Nby-1)

    allocate(aij(1:Nbx,1:Nby))
    print *,lbound(aij),ubound(aij)

    aij(1,1)=AAij(1,1)

    do ii=2, Nbx-1
        aij(ii,1)=AAij(ii,1) - Bi(ii)*(Ei(ii-1)+ Fj(1))/aij(ii-1,1)
    end do

    do jj=2, Nby-1
        aij(1,jj)=AAij(1,jj) - Cj(jj)*(Fj(jj-1)+ Ei(1))/aij(1,jj-1)
        do ii=2,Nbx-1
            aij(ii,jj) = AAij(ii,jj)- Bi(ii)*(Ei(ii-1)+ Fj(jj) )/aij(ii-1,jj) - Cj(jj)*(Fj(jj-1)+ Ei(ii))/aij(ii,jj-1)
        end do
    end do
end subroutine constructFD

!---------------------------------------------------
real*8 function AAij(i,j)
    implicit none
    integer, intent(in) :: i, j

    !AAij=-2d0*(1.d0/(dxi(i)*dxi(i+1)) + 1.d0/(dyj(j)*dyj(j+1)))
    AAij=-2d0*(1.d0/(dxi(i)*dxi(i+1)))
end function AAij
!---------------------------------------------------

!---------------------------------------------------
real*8 function Bi(i)
    implicit none
    integer, intent(in) :: i

    Bi=2.d0/(dxi(i)*(dxi(i) + dxi(i+1)))
end function Bi
!---------------------------------------------------

!---------------------------------------------------
real*8 function Cj(j)
    implicit none
    integer, intent(in) :: j

    !Cj=2.d0/(dyj(j)*(dyj(j) + dyj(j+1)))
    Cj=0.d0
end function Cj
!---------------------------------------------------

!---------------------------------------------------
real*8 function Ei(i)
    implicit none
    integer, intent(in) :: i

    Ei=2.d0/(dxi(i+1)*(dxi(i) + dxi(i+1)))
end function Ei
!---------------------------------------------------

!---------------------------------------------------
real*8 function Fj(j)
    implicit none
    integer, intent(in) :: j

    !Fj=2.d0/(dyj(j+1)*(dyj(j) + dyj(j+1)))
    Fj=0.d0
end function Fj
!---------------------------------------------------

!---------------------------------------------------
subroutine Solve(Rij, uij)
    implicit none
    real*8, intent(in) :: Rij(1:,1:)
    real*8, intent(inout) :: uij(1:,1:)

    real*8 :: w(Nbx,Nby)
    integer :: ii, jj, N,M
    N=Nbx
    M=Nby

    w(1,1)=Rij(1,1)/aij(1,1)

    do ii=2,N-1
        w(ii,1)=(Rij(ii,1)-Bi(ii)*w(ii-1,1))/aij(ii,1)
    end do

    do jj=2, M-1
        w(1,jj)=(Rij(1,jj)-Cj(jj)*w(1,jj-1))/aij(1,jj)
        do ii=2,N-1
            w(ii,jj)=(Rij(ii,jj)-Bi(ii)*w(ii-1,jj)-Cj(jj)*w(ii,jj-1))/aij(ii,jj)
        end do
    end do

    uij(N-1,M-1)=w(N-1,M-1)
    !TODO inverser pour lecture colone?
    do ii=N-2, 1, -1
        uij(ii,M-1)=w(ii,M-1) -  Ei(ii)*uij(ii+1,M-1)/aij(ii,M-1)
    end do
    do jj=M-2,1,-1
        uij(N-1,jj)=w(N-1,jj) -  Fj(jj)*uij(N-1,jj+1)/aij(N-1,jj)
        do ii=N-2,1,-1
            uij(ii,jj)=w(ii,jj)-( Ei(ii)*uij(ii+1,jj) + Fj(jj)*uij(ii,jj+1)) /aij(ii,jj)
        end do
    end do

end subroutine
!---------------------------------------------------

!---------------------------------------------------
subroutine ConjugateGradientSolve(Nit,TOL,sol,RHS, deltat)
    use SEM1D, only : Residual, MatrixAction
    implicit none
    integer,intent(in) :: Nit
    real*8, intent(in) :: TOL, deltat
    real*8, intent(inout) :: sol(0:,1:)
    real*8, intent(inout) :: RHS(0:,1:)

    real*8, allocatable :: resNK(:,:),zNK(:,:), vNK(:,:)
    real*8 :: c,w,d

    integer :: kk,lN,uN,lK,uK
    lN=lbound(sol,1)
    uN=ubound(sol,1)
    lK=lbound(sol,2)
    uK=ubound(sol,2)

    allocate(resNK(lN:uN,lK:uK))
    allocate(  vNK(lN:uN,lK:uK))
    allocate(  zNK(lN:uN,lK:uK))

    call Residual(sol, resNK, deltat)
    !call Solve(resNK,zNK)
    zNK=resNK
    vNK=zNK
    c=sum(resNK*zNK)

    do kk=1,Nit
        call MatrixAction(-1.d0,deltat, vNK, zNK)
        w=c/sum(vNK*zNK)
        sol=w*vNK+sol
        resNK=-w*zNK + resNK
        if (sqrt(sum(resNK*resNK)) <TOL) exit
        !call Solve(resNK,zNK)
        zNK=resNK
        d=sum(resNK*zNK)
        vNK=zNK + d/c*vNK
        c=d
    end do
    write (*,'(*(G0.6,1X))') 'Iteration:', kk,'residut:',sqrt(sum(resNK*resNK))


end subroutine ConjugateGradientSolve

!---------------------------------------------------
end module SolverPack
