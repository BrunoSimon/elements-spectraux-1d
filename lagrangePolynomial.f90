!Module for Lagrange polynomial
module lagrangePoly
implicit none
private
public :: BarycentricWeights, printLag, GNN, CGDerivativeMatrix
public :: LagrangeInterpolation, verifDerivative

real*8, allocatable :: lagWeight_n(:) !0-NbNode
real*8, allocatable :: DNN(:,:)
real*8, allocatable :: GNN(:,:)

contains
!-----------------------------
subroutine BarycentricWeights(x)
    implicit none
    real*8, intent(in) :: x(0:)

    integer :: jj,kk, N

    N=size(x)-1
    allocate(lagWeight_n(0:N))

    lagWeight_n=1.D0
    do jj=1,N
        do kk=0,jj-1
            lagWeight_n(kk)=lagWeight_n(kk)*(x(kk)-x(jj))
            lagWeight_n(jj)=lagWeight_n(jj)*(x(jj)-x(kk))
        end do
    end do
    lagWeight_n=1.D0/lagWeight_n

end subroutine BarycentricWeights
!-----------------------------

!-----------------------------
real*8 function LagrangeInterpolation(x, xj, fj)
    use procMath, only: AlmostEqual
    implicit none

    real*8, intent(in) :: x
    real*8, intent(in) :: xj(0:),fj(0:)

    real *8 :: num, denum
    real *8 :: t
    integer :: jj, N

    N=size(xj)-1
    call BarycentricWeights(xj)

    num=0.d0
    denum=0.D0

    do jj=0,N
        if (AlmostEqual(x,xj(jj))) then
            LagrangeInterpolation=fj(jj)
            return
        end if
        t=lagWeight_n(jj)/(x-xj(jj))
        num=num+t*fj(jj)
        denum=denum+t
    end do
    LagrangeInterpolation=num/denum
    deallocate(lagWeight_n)
end function LagrangeInterpolation
!-----------------------------


!-----------------------------
subroutine PolynomialInterpolationMatrix(xN,ksiM, TMN)
    use procMath, only : AlmostEqual
    implicit none
    real*8, intent(in) :: xN(0:),ksiM(0:)
    real*8, intent(inout) :: TMN(0:,0:)

    logical :: rowHasMatch
    integer :: kk, jj, N, M
    real*8 :: s,t

    N=size(xN)-1
    M=size(ksiM)-1
    call BarycentricWeights(xN)

    do kk=0,M
        rowHasMatch=.false.
        do jj=0,N
            TMN(kk,jj)=0.d0
            if (AlmostEqual(ksiM(kk),xN(jj))) then
                TMN(kk,jj)=1.d0
                rowHasMatch=.true.
            end if
        end do

        if (.not. rowHasMatch) then
            s=0.d0
            do jj=0, N
                t=lagWeight_n(jj)/(ksiM(kk)-xN(jj))
                TMN(kk,jj)=t
                s=s+t
            end do
            TMN(kk,:)=TMN(kk,:)/s
        end if
    end do
    deallocate(lagWeight_n)
end subroutine PolynomialInterpolationMatrix
!-----------------------------


!-----------------------------
subroutine InterpolateToNewPoints(TMN,fN,finterpM)
    use procMath, only : AlmostEqual
    implicit none
    real*8, intent(in) :: TMN(0:,0:),fN(0:)
    real*8, intent(inout) :: finterpM(:)

    integer :: ii,jj,M,N
    real*8 :: t

    N=size(fN)-1
    M=size(TMN(:,0))-1

    finterpM=matmul(TMN,fN)
    do ii=0,M
        t=0.
        do jj=0,N
            t=t+TMN(ii,jj)*fN(jj)
        end do
        if (.not. AlmostEqual(finterpM(jj),t)) then
            print *,"oupsi"
            exit
        end if
    end do

end subroutine InterpolateToNewPoints
!-----------------------------


!-----------------------------
subroutine PolynomialDerivativeMatrix(x)
    implicit none
    real*8, intent(in) :: x(0:)

    integer :: jj,N,ii

    N=size(x)-1
    allocate(DNN(0:N,0:N))

    call BarycentricWeights(x)

    do ii=0, N
        DNN(ii,ii)=0.d0
        do jj=0, N
            if (ii/=jj) then
                DNN(ii,jj)=lagWeight_n(jj) /(lagWeight_n(ii) * (x(ii)-x(jj)))
                DNN(ii,ii)=DNN(ii,ii)-DNN(ii,jj)
            end if
        end do
    end do
    deallocate(lagWeight_n)
end subroutine PolynomialDerivativeMatrix
!-----------------------------

!-----------------------------
subroutine CGDerivativeMatrix(N)
    !Algorithm 57
    use quadrature, only : getXNode, getWeight
    implicit none
    integer, intent(in) :: N

    real*8 :: x(0:N), w(0:N),s
    integer ::jj,kk,nn

    x=getXNode()
    w=getWeight()

    call PolynomialDerivativeMatrix(x)

    allocate(GNN(0:N,0:N))

    do jj=0,N
        do  nn=0, N
            s=0.d0
            do kk=0, N
                s=s+DNN(kk,nn)*DNN(kk,jj)*w(kk)
            end do
            GNN(jj,nn)=s
        end do
    end do

end subroutine CGDerivativeMatrix
!-----------------------------

!-----------------------------
subroutine printLag()
    implicit none
    print *, "Lagrange weight:"
    print *, lagWeight_n
end subroutine printLag
!-----------------------------

!-----------------------------
subroutine verifDerivative(x)
    use procMath, only: AlmostEqual
    implicit none
    real*8, intent (in) :: x(0:)

    integer :: ii,N
    real*8, allocatable :: f(:), fp(:)

    N=size(x)-1
    allocate(f(0:N))
    allocate(fp(0:N))

    do ii=0,N
        f(ii)=x(ii)**real(N,8)
    end do

    call PolynomialDerivativeMatrix(x)

    do ii=0,N
        fp(ii)=dot_product(DNN(ii,:),f)
        if (AlmostEqual(fp(ii),N*x(ii)**(N-1))) then
            print *, "derivative Ok"
        else
            print *, "derivative not Ok"
            print *,  fp(ii),N*x(ii)**(N-1)
        end if
    end do

    deallocate(DNN)
    deallocate(f)
    deallocate(fp)

end subroutine
!-----------------------------
end module lagrangePoly
!-----------------------------
