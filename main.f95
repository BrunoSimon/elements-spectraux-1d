program main
    !See Chapitre 8.1 of Kopriva
    use SEM1D, only : constructSEM1D, printInfo, sol_Init
    use SEM1D, only : sol_NK, rhs_NK, xReal
    use SEM1D, only : MatrixAction, SetBoundaryConditions, UnMask
    use quadrature, only : getXNode
    use SolverPack, only : ConjugateGradientSolve, constructFD

    implicit none
    character(len=10) :: quadMet,cNt

    integer :: nBDom, NbNode, NT, Nit, nn
    integer :: jj, kk
    real*8 :: xFrontDom(0:3)
    !real*8 :: u(2),m(3,2)
    real*8 :: deltat,t, TOL

    !Initialisation du la methode:
    !pas de temps
    deltat=0.05D0

    !Domaine et sous segments
    NbDom=3     !Nombre de sous segment/domaine
    xFrontDom=(/-8.d0, -3.d0, 3.d0, 8.d0/)    !liste des points des sous intervalles du domaine

    !nombre de noeud pour la quadrature et base polynomiale
    NbNode=10
    quadMet="LGL"

    !Nombre d iteration en temps
    NT=4*20
    !nombre d iteration et precision solver gradient conjugu�e
    Nit=20
    TOL=1.d-12

    !construction de la base polynomial, et de la matrice de derivation
    call constructSEM1D(NbNode,nBDom,xFrontDom,quadMet)

    !construction des elements pour le gradient conjugu�e
    !call constructFD(NbNode,NbDom,getXNode(),xFrontDom)

    call sol_Init(sol_NK)

    !save infile init condition
    OPEN(UNIT=13, FILE='sol_init.dat', ACTION="write", STATUS="replace")
    DO kk=1,nBDom
        do jj=min(0,kk-1),NbNode !(exclude duplicate at shared boundaries)
            write(13,'(*(G0.12,1X))') xreal(jj,kk), sol_NK(jj,kk)
        end do
    END DO !tracer angle
    close(13)

    write(*,*) "Time loop"
    !Time loop
    do nn=0, NT-1
        t=nn*deltat
        write(*,'(*(G0,1X))') "iteration:", nn, "time", t, "s"

        call MatrixAction(1.d0,deltat, sol_NK, rhs_NK)
        !write(cNt,'(i2)') nn

!        OPEN(UNIT=14, FILE='sol_inter'//cNt(1:len_trim(cNt))//'.dat', ACTION="write", STATUS="replace")
!        DO kk=1,nBDom
!            do jj=min(0,kk-1),NbNode !(exclude duplicate at shared boundaries)
!                WRITE(14,'(*(G0.12,1X))') xreal(jj,kk), sol_NK(jj,kk)
!            end do
!        END DO !tracer angle
!        close(14)

        call SetBoundaryConditions(sol_NK,t+deltat)

        call ConjugateGradientSolve(Nit,TOL,sol_NK,rhs_NK, deltat)
    end do

    t=t+deltat
    write(*,'(*(G0.6,1X))') "Temps final", t, "s"
    call UnMask(sol_NK)

    !write final result
    OPEN(UNIT=15, FILE='sol_end.dat', ACTION="write", STATUS="replace")
    DO kk=1,nBDom
        do jj=min(0,kk-1),NbNode !(exclude duplicate at shared boundaries)
            write(15,'(*(G0.12,1X))') xreal(jj,kk), sol_NK(jj,kk)
        end do
    END DO !tracer angle
    close(15)

    write(*,*) "The end!"
end program
