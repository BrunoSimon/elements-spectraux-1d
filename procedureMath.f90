module procMath
    implicit none

    contains
!-----------------------------
    logical function AlmostEqual(a,b)
    implicit none
    real*8,intent(in) :: a,b

    if ((a==0.d0) .or. (b==0.d0)) then
        AlmostEqual=abs(a-b)<= 2.d0*epsilon(a)
    else
        AlmostEqual= abs(a-b) <= 2.d0*epsilon(a) ! *min(abs(a),abs(b))
    end if
end function AlmostEqual
!-----------------------------
end module procMath
