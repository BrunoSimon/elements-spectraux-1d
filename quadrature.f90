!Module with quadrature formula
!TODO Write subroutine verifying the node and weight
module quadrature
implicit none

private
public :: NodesAndWeight,printNodesAndWeights
public :: getNbNode, getXNode,getXNoden, getWeight, NbNode
public :: weight_n

integer :: NbNode       !Number of node in an element N

real*8, allocatable :: xnode_n(:)   !0-NbNode
real*8, allocatable :: weight_n(:) !0-NbNode
real*8, parameter :: pi=4.D0*atan(1.D0)
character(len=10) :: quadUse

contains

!-------------------------------------
subroutine NodesAndWeight(N,method)
    !Compute nodes and weight using the selected method
    implicit none

    integer,intent(in) :: N
    character(len=10),intent(in) :: method

    NbNode=N
    quadUse=method
    allocate(xnode_n(0:NbNode))
    allocate(weight_n(0:NbNode))

    SELECT CASE (TRIM(quadUse))
        CASE ('LGL')
            write (*,*) "Using Legendre Gauss Lobatto quadrature"
            call LegendreGaussLobattoNodesAndWeights(N)
        CASE default
            write(*,*) "The quadrature method does not exist!"
    END SELECT
end subroutine NodesAndWeight
!-------------------------------------

!-------------------------------------
subroutine qAndLEvaluation(N,x,q,qprime,LN)
    !compute LN(x), q(=L{N+1}-L{N-1}) and qprime
    !Combined method to find q and L

    implicit none

    integer, intent(in) :: N
    real*8, intent(in) :: x
    real*8, intent(out) :: q
    real*8, intent(out) :: qprime
    real*8, intent(out) :: LN

    integer :: k
    real*8 :: LNMoins2,LNMoins1,LNPlus1
    real*8 :: LpNMoins2,LpNMoins1,LpN,LpNPlus1

    LNMoins2=1.
    LNMoins1=x
    LpNMoins2=0.
    LpNMoins1=1.

    do k=2,N
        LN=( (2.d0*k-1.)*x*LNMoins1 - (k-1.d0)*LNMoins2 )/k
        LpN=LpNMoins2 + (2.d0*k-1.d0)*LNMoins1
        LNMoins2=LNMoins1
        LNMoins1=LN
        LpNMoins2=LpNMoins1
        LpNMoins1=LpN
    end do
    k=N+1
    LNPlus1=( (2.d0*k-1.d0)*x*LN - (k-1.d0)*LNMoins2 )/k
    LpNPlus1=LpNMoins2 + (2.d0*k-1.d0)*LNMoins1

    q=LNPlus1-LNMoins2
    qprime=LpNPlus1-LpNMoins2
end subroutine qAndLEvaluation
!-------------------------------------

!-------------------------------------
subroutine LegendreGaussLobattoNodesAndWeights(N)
    !Compute nodes and weight using for the Legendre Gauss Lobatto quad
    implicit none

    integer,intent(in) :: N

    integer :: jj,kk
    real*8 :: delta,TOL, q, qprime, LN

    if (N==1) then
        xnode_n  =(/-1.d0, 1.d0/)
        weight_n=(/1.d0, 1.d0 /)
    else
        xnode_n(0)=-1.d0
        xnode_n(N)=1.d0
        weight_n(0)=2.d0/(N*(N+1.d0))
        weight_n(N)=weight_n(0)
        do jj=1, (N+1)/2 - 1
            xnode_n(jj)=-cos((jj+0.25d0)*pi/N - 3.d0/(8.d0*N*pi*(jj+0.25d0)))
            kk=0
            delta=1.d0
            do kk=0,5
                call qAndLEvaluation(N,xnode_n(jj),q,qprime,LN)
                delta=-q/qprime
                xnode_n(jj)=xnode_n(jj)+delta
                TOL=epsilon(delta)*4.d0
                if (abs(delta)<=TOL*abs(xnode_n(jj))) exit
            end do
            call qAndLEvaluation(N,xnode_n(jj),q,qprime,LN)
            xnode_n(N-jj)=-xnode_n(jj)
            weight_n(jj)=2.d0/(N*(N+1.d0)*LN*LN)
            weight_n(N-jj)=weight_n(jj)
        end do
    end if
    if(modulo(N,2)==0) then
        xnode_n(N/2)=0.d0
        call qAndLEvaluation(N,xnode_n(N/2),q,qprime,LN)
        weight_n(N/2)=2.d0/(N*(N+1.d0)*LN*LN)
    end if
end subroutine LegendreGaussLobattoNodesAndWeights
!-------------------------------------

!-------------------------------------
subroutine printNodesAndWeights()
    !Print the values of nodes and weight
    implicit none
    print *, "Method:", quadUse
    print *, "Nombre de noeud",NbNode+1
    print *, "Nodes",xnode_n
    print *, "Weights",weight_n
end subroutine printNodesAndWeights
!-------------------------------------


!-------------------------------------
subroutine setNbNode(N)
    !Set number of Node
    implicit none
    integer,intent(in) :: N
    NbNode=N
end subroutine setNbNode
!-------------------------------------

!-------------------------------------
integer function getNbNode()
    !get the number of Node
    implicit none
    getNbNode=NbNode
end function getNbNode
!-------------------------------------

!-------------------------------------
function getXNode()
    implicit none
    real*8, dimension(NbNode+1) :: getXNode

    getXNode=xnode_n
end function getXNode
!-------------------------------------

!-------------------------------------
function getXNoden(num_node)
    implicit none
    real*8 :: getXNoden
    integer, intent(in) :: num_node

    getXNoden=xnode_n(num_node)

end function getXNoden
!-------------------------------------

!-------------------------------------
function getWeight()
    implicit none
    real*8, dimension(NbNode+1) :: getWeight

    getWeight=weight_n

end function getWeight
!-------------------------------------


!-------------------------------------
subroutine test_quadrature(N)
    !Unit test if quadrature is correct for x^n
    use procMath, only : AlmostEqual
    implicit none

    integer, intent(in) :: N
    integer :: jj, kk
    real*8 :: integral,exact

    do jj=1,2*N+1 !supposed to be exact to only 2*n-1 for lobatto point
        integral=0.d0
        do kk=0,N
            integral=integral+xnode_n(kk)**jj * weight_n(kk)
        end do
        exact=1.d0/(jj+1.d0)*(1.d0**(jj+1)-(-1.d0)**(jj+1))
        if (AlmostEqual(integral,exact)) then
            print *, "Ok!"
            print *, "integral, exact"
            print *, integral,exact
        else
            print *, "Problem quadrature"
            print *, "integral, exact"
            print *, integral,exact
        end if
    end do
end subroutine
!-------------------------------------
end module quadrature
!-------------------------------------
