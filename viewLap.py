# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 12:11:39 2019

@author: Bruno
"""



#clean console Ipython
from IPython import get_ipython
get_ipython().magic('reset -sf')

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt


mpl.pyplot.close("all")

rep=".\\"
filepatern1="sol_end.dat"
filename1=rep+filepatern1

my_data1=np.genfromtxt(filename1,dtype=float, comments='#')
#
t=4
#x=my_data1[:,0]
x=np.linspace(my_data1[0,0],my_data1[-1,0] ,501)

f1=np.exp(-x**2/(4*t+1))/np.sqrt(4*t+1)
gradf1=np.gradient(f1)


#plt.figure(figsize=(15, 8))
plt.plot(my_data1[:,0], my_data1[:,1:],marker='+')
plt.plot(x, f1 )

plt.legend(['fSEM','fexact'])
plt.xlabel('x m')
plt.ylabel(r'$\phi$')
plt.show()

plt.savefig('diffusion4s.png', dpi=300, bbox_inches='tight')